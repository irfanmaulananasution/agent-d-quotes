package com.agentd.tugasservice.controller;

import com.agentd.tugasservice.core.TugasIndividu;
import com.agentd.tugasservice.core.UserAgentD;
import com.agentd.tugasservice.service.TugasService;
import org.apache.catalina.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.handler;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = TugasServiceController.class)
public class TugasServiceControllerTests {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private TugasService tugasService;

    @Test
    public void whenRegisterTugasURIIsAccessedItShouldHandledByRegisterPenggunaById() throws Exception {
        mockMvc.perform(post("/tugas/register-user/123abc")).andExpect(status().isOk()).andExpect(handler().methodName("registerPenggunaById"));
    }

    @Test
    public void whenFindTugasURIIsAccessedItShouldHandledByFindPenggunaById() throws Exception {
        mockMvc.perform(get("/tugas/find-user/123abc")).andExpect(status().isOk()).andExpect(handler().methodName("findPenggunaById"));
    }

    @Test
    public void testMethodregisterTugas() throws Exception {
        mockMvc.perform(post("/tugas/register-tugas/123abc/adpro/tugas1/02-02-2020")).andExpect(status().isOk()).andExpect(handler().methodName("registerTugasUntukPengguna"));
    }

    @Test
    public void testMethodLihatTugas() throws Exception {
        mockMvc.perform(get("/tugas/lihat-tugas/123abc")).andExpect(status().isOk()).andExpect(handler().methodName("findAllTugasIndividu"));
    }
}
