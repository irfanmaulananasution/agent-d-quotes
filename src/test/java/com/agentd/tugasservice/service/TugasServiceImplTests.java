package com.agentd.tugasservice.service;

import com.agentd.tugasservice.core.TugasIndividu;
import com.agentd.tugasservice.core.UserAgentD;
import org.apache.catalina.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class TugasServiceImplTests {
    TugasServiceImpl service;
    UserAgentD user;
    TugasIndividu tugas;

    @BeforeEach
    public void setUp(){
        service = new TugasServiceImpl();
        user = new UserAgentD("123abc");
        Date deadline = new Date(2020,02,02);
        tugas = new TugasIndividu("adpro","tugas1",deadline);
    }

    @Test
    public void testMethodRegisterUser(){
        service.registerUser(user);
        assertEquals(1,service.getRepository().getRepo().size());
    }

    @Test
    public void testMethodFindUser(){
        service.registerUser(user);
        assertEquals(user,service.findUser("123abc"));
    }

    @Test
    public void testMethodRegisterTugas(){
        service.registerUser(user);
        service.registerTugas(user.getId(),"adpro","tugas","02-02-2020");
        assertEquals(1,service.findUser(user.getId()).lihatTugasIndividu().size());
    }

    @Test
    public void testMethodFindAllTugasIndividu(){
        service.registerUser(user);
        service.registerTugas(user.getId(),"adpro","tugas","02-02-2020");
        assertEquals(1,service.findAllTugasOfUser("123abc").size());
    }
}