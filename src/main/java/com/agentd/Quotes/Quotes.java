package com.bot.agentd;

import java.io.*;
import java.util.ArrayList;

public class Quotes {
    ArrayList<String> quotesRepo;
    static int quotesRepoIndexPointer;
    int repoSize;

    Quotes() {
        try {
            this.quotesRepo = this.repoInit();
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.quotesRepoIndexPointer = 0;
        this.repoSize = this.quotesRepo.size();
        System.out.println(repoSize);
    }

    public static ArrayList<String> repoInit() throws IOException {
        ArrayList<String> quotesRepo = new ArrayList<String>();
        File file = new File("daftarQuotes.txt");
        BufferedReader br = new BufferedReader(new FileReader(file));

        String tmp;
        while((tmp = br.readLine()) != null) {
            quotesRepo.add(tmp);
        }
        return quotesRepo;
    }

    public String getQuote(){
        String quote = this.quotesRepo.get(this.quotesRepoIndexPointer);
        this.quotesRepoIndexPointer = (this.quotesRepoIndexPointer+1) % (this.repoSize);
        return quote;
    }
}
